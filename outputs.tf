# outputs.tf defines outputs for the module.

output "topic_id" {
  description = "The ID of the created or supplied Pub/Sub topic."
  value       = local.topic_id
}
