# main.tf configures top-level resources

resource "random_id" "topic_name" {
  count = var.create_default_topic ? 1 : 0

  byte_length = 4
  prefix      = "pubsub2teams-"
  keepers     = {}
}

resource "google_pubsub_topic" "default" {
  count = var.create_default_topic ? 1 : 0

  project = var.project
  name    = random_id.topic_name[0].hex
}

resource "google_pubsub_topic_iam_member" "publishers" {
  for_each = toset(var.topic_publishers)

  project = var.project
  topic   = local.topic_id
  role    = "roles/pubsub.publisher"
  member  = each.value
}

locals {
  # If no topic_id is specified, use the default topic id.
  topic_id = var.topic_id == null ? google_pubsub_topic.default[0].id : var.topic_id
}

# This function is deployed to the specified project. It will proxy messages published to the specified Pub/Sub
# topic to one or more product-specific MS Teams channels. An Incoming Webhook connector(s) will need to be configured
# manually in the Teams channel and passed as variables.
module "pubsub2teams" {
  for_each = var.msteams_channel_webhook_urls

  source      = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function.git?ref=v2"
  name        = "${each.key}-pubsub2teams"
  description = "A function to proxy the pubsub topic to an MS Teams channel."
  project     = var.project
  region      = var.region
  runtime     = "python311"

  event_trigger = {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource   = local.topic_id
  }

  source_files = {
    "requirements.txt"          = file("${path.module}/cloud-function/requirements.txt")
    "main.py"                   = file("${path.module}/cloud-function/main.py")
    "eventhandlers/__init__.py" = file("${path.module}/cloud-function/eventhandlers/__init__.py")
    "eventhandlers/alerts.py"   = file("${path.module}/cloud-function/eventhandlers/alerts.py")
    "eventhandlers/cards.py"    = file("${path.module}/cloud-function/eventhandlers/cards.py")
    "eventhandlers/factory.py"  = file("${path.module}/cloud-function/eventhandlers/factory.py")
    "eventhandlers/gke.py"      = file("${path.module}/cloud-function/eventhandlers/gke.py")
    "eventhandlers/handler.py"  = file("${path.module}/cloud-function/eventhandlers/handler.py")
  }

  environment_variables = {
    MSTEAMS_WEBHOOK_URL = each.value
  }
}

# Allow pubsub2teams cloud function SA to view project information (resourcemanager.projects.get)
resource "google_project_iam_member" "pubsub2teams" {
  for_each = module.pubsub2teams

  project = var.project
  role    = "roles/browser"
  member  = "serviceAccount:${each.value.service_account.email}"
}

resource "google_monitoring_alert_policy" "pubsub2teams" {
  for_each = var.msteams_channel_webhook_urls

  display_name = "Error in pubsub2teams function execution for function '${module.pubsub2teams[each.key].function.name}'"
  project      = var.project
  combiner     = "OR"

  notification_channels = var.email_notification_channels

  conditions {
    display_name = "An error occurred in the execution of the ${module.pubsub2teams[each.key].function.name} cloud function"

    condition_matched_log {
      filter = <<EOI
resource.type="cloud_function"
resource.labels.function_name="${module.pubsub2teams[each.key].function.name}"
severity=ERROR
EOI
    }
  }

  alert_strategy {
    auto_close = "604800s" # 7 days

    notification_rate_limit {
      period = "3600s" # 1 hour
    }
  }

  documentation {
    content = <<EOI
This is a catch all alert for any errors that occur in the pubsub2teams Cloud Function execution.

Initial investigation steps:

- Go to Cloud Functions in the Google Cloud Console.
- Select the specific function that this alert relates to.
- Click "Logs" in the navigation tabs.
- Look for errors in the logs that occurred around the same time that this alert was triggered.
EOI
  }
}
