# variables.tf defines inputs for the module

variable "msteams_channel_webhook_urls" {
  description = <<EOI
Specify one or more MS Teams Webhook Connector URLs. A Cloud Function will deployed for each URL to proxy messages
sent to the Pub/Sub topic to the MS Teams webhook. For more information on how this works see
./functions/pubsub2teams/README.md.

An example of this variable is as follows. Note the key names cannot contain underscores.

msteams_channel_webhook_urls = {
  "cloud-team" = "<msteams webhook connector url>"
}
EOI
  type        = map(string)
  default     = {}
}

variable "project" {
  description = "Project containing the cloud function."
  type        = string
}

variable "region" {
  description = "Region to create resources in."
  type        = string
  default     = "europe-west2"
}

variable "create_default_topic" {
  description = <<EOI
When true, a randon topic will be created. and used unless a topic_id is specified.
When false, the topic_id must be specified.
EOI
  type        = bool
  default     = true
}

variable "topic_id" {
  description = <<EOI
Optional topic id of a pre-existing topic to use.
Topic id must be in the format projects/{{project}}/topics/{{name}}.
If no topic id is specified, the generated default topic will be used.
EOI
  type        = string
  default     = null
}

variable "topic_publishers" {
  description = <<EOI
Optional groups, service accounts or users who can publish to the pubsub notification topic.
Each will be given the pubsub.publisher role for the topic.
The "group:", "serviceAccount" or "user:"  prefix must be used, for example:

[
  "group:<some group name>",
  "serviceAccount:<some service account name>",
  "user:<some user name>"
]
EOI
  type        = list(string)
  default     = []
}

variable "email_notification_channels" {
  description = <<EOI
Optional list of notification channel id's to send alerts to.
The syntax of the entries in this field is 'projects/[PROJECT_ID]/notificationChannels/[CHANNEL_ID]'
NOTE: Be careful not to add the channel id of the notification which publishes to the pubsub topic,
which triggers the cloud function, as this will create a loop - only include email channel(s).
EOI
  type        = list(string)
  default     = []
}
