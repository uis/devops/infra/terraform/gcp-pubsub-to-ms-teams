# GCP PubSub To MS Teams

This is a function to proxy messages from a pubsub topic to a specified MS Teams channel via a configured Incoming
Webook connector. The module creates a PubSub topic and returns the id as an output variable.

## General configuration

1. Create an [Incoming Webhook
   Workflow](https://support.microsoft.com/en-us/office/creating-a-workflow-from-a-channel-in-teams-242eb8f2-f328-45be-b81f-9817b51a5f0e)
   for the required MS Teams workflow and make a note of the generated URL.

2. Configure your Terraform to use the module:

   ```tf

    module "my_notifications" {
      source  = "gitlab.developers.cam.ac.uk/uis/gcp-pubsub-to-ms-teams/devops"
      version = "1.0.0"

      project = local.project
      region  = local.region

      msteams_channel_webhook_urls = "<your webhook url>"

      topic_publishers = [
        "serviceAccount:<some service account name>"
      ]
    }

   ```

## Local development

### Pre-commit

This project makes use of [pre-commit](https://pre-commit.com/). Ensure that you have installed and
enabled pre-commit as per [the documentation](https://pre-commit.com/#install) to avoid nasty CI
surprises.

Install git pre-commit hooks via:

```console
pip install pre-commit
pre-commit install
```

To run pre-commit manually:

```console
pre-commit run --all-files
```

## Code Overview

The Cloud Function subscribes to the configured topic and receieves an event. The event can be any type
of message, but obviously the function needs to be aware of the types of messages it will receive.

Due to the diverse nature of GCP notification messages, a `Factory` class takes the message and asks each of the
configured `Handlers` if they can handle the message. If a handler can handle the message, it is passed
to that handler for processing and the handler is returned by the factory. If no handler can handle the
message, it is logged and an error is generated, although this in theory should never happen, as we
shouldn't configure any messages to be published to the topic that we don't expect the function to handle.

There are two types of messages that the function can handle currently:

- GCP Alert Notification messages
- GKE Cluster noticification messages

### GCP Alert Notification messages

GCP Alerting policies can have zero or more notification channels configured. One of the supported notification
channels is PubSub, the format of which is:

- [Alert notification message format](https://cloud.google.com/monitoring/support/notification-options#schema-pubsub)

### GKE Cluster noticification messages

Google Kubernetes Engine (GKE) can publish cluster notifications to PubSub with information about events relevant
to a cluster, such as available upgrades and security bulletins. The generic format of these messages is:

- [Cluster notification message format](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications#modes)

Depending on the `type_url` attribute in the message, the payload will be one of these types:

- [SecurityBulletinEvent](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications#securitybulletinevent_2)
- [UpgradeAvailableEvent](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications#upgradeavailableevent_2)
- [UpgradeEvent](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications#upgradeevent_2)
