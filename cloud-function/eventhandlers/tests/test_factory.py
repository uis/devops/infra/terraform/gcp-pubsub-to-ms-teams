import base64
import json
from unittest import TestCase
from eventhandlers.alerts import AlertEventHandler
from eventhandlers.gke import SecurityBulletinEventHandler, UpgradeAvailableEventHandler, UpgradeEventHandler

from eventhandlers.factory import EventHandlerFactory


class TestFactoryAlertEvent(TestCase):

    def setUp(self):
        self.factory = EventHandlerFactory()

    def test_alert_event(self):
        data = {
            "incident": {
                "incident_id": "test"
            },
            "version": "1.2"
        }
        event = {
            "data": base64.b64encode(json.dumps(data).encode("utf-8"))
        }

        self.assertIsInstance(self.factory.get_event_handler(event), AlertEventHandler)

    def test_security_bulletin_event(self):
        event = {
            "attributes": {
                "project_id": "my-project",
                "cluster_location": "europe-west2-a",
                "cluster_name": "my-cluster",
                "type_url": "type.googleapis.com/google.container.v1beta1.SecurityBulletinEvent",
                "payload": {
                    "cluster_name": "my-cluster"
                }
            },
            "data": base64.b64encode("Just some text".encode("utf-8"))
        }

        self.assertIsInstance(self.factory.get_event_handler(event), SecurityBulletinEventHandler)

    def test_upgrade_available_event(self):
        event = {
            "attributes": {
                "project_id": "my-project",
                "cluster_location": "europe-west2-a",
                "cluster_name": "my-cluster",
                "type_url": "type.googleapis.com/google.container.v1beta1.UpgradeAvailableEvent",
                "payload": {
                    "cluster_name": "my-cluster"
                }
            },
            "data": base64.b64encode("Just some text".encode("utf-8"))
        }

        self.assertIsInstance(self.factory.get_event_handler(event), UpgradeAvailableEventHandler)

    def test_upgrade_event(self):
        event = {
            "attributes": {
                "project_id": "my-project",
                "cluster_location": "europe-west2-a",
                "cluster_name": "my-cluster",
                "type_url": "type.googleapis.com/google.container.v1beta1.UpgradeEvent",
                "payload": {
                    "cluster_name": "my-cluster"
                }
            },
            "data": base64.b64encode("Just some text".encode("utf-8"))
        }

        self.assertIsInstance(self.factory.get_event_handler(event), UpgradeEventHandler)

    def test_unknown_event(self):
        data = base64.b64encode("blah".encode("utf-8"))
        event = {
            "attributes": {
                "a1": "a1"
            },
            "data": data
        }

        with self.assertRaises(Exception) as context:
            self.factory.get_event_handler(event)
        self.assertEqual(str(context.exception), "No event handler found for event.")
