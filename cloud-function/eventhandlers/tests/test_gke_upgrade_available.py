import base64
import json
import os
from unittest import TestCase, mock

from eventhandlers.gke import UpgradeAvailableEventHandler

script_dir = os.path.dirname(os.path.abspath(__file__))


class TestUpgradeAvailableEventHandler(TestCase):
    def setUp(self):
        with open(os.path.join(script_dir, 'gke_upgrade_available_event.json'), 'r') as json_file:
            self.payload = json.load(json_file)

        self.test_event = {
            "attributes": {
                "project_id": "123456789012",
                "cluster_location": "europe-west2",
                "cluster_name": "cluster",
                "type_url": "type.googleapis.com/google.container.v1beta1.UpgradeAvailableEvent",
                "payload": json.dumps(self.payload)
            },
            "data": base64.b64encode(
                'New node version "1.17.15-gke.800" is available for upgrade in the REGULAR channel.'.encode(
                    "utf-8"
                )
            ),
        }

    def test_security_bulletin_event(self):
        handler = UpgradeAvailableEventHandler(self.test_event)

        self.assertTrue(handler.can_handle_event())

    def test_non_security_bulletin_event(self):
        event = {
            "data": base64.b64encode("string message".encode("utf-8"))
        }

        handler = UpgradeAvailableEventHandler(event)

        self.assertFalse(handler.can_handle_event())

    @mock.patch("eventhandlers.gke.GKEMessageCard._get_project_info", autospec=True)
    def test_to_json(self, mock_get_project_info):
        mock_get_project_info.return_value = {
            "display_name": "Name - development",
            "project_id": "blah-devel-72a2b0bc",
        }

        handler = UpgradeAvailableEventHandler(self.test_event)

        with open(os.path.join(script_dir, 'gke_upgrade_available_adaptive_card.json'), 'r') as json_file:
            expected_json = json.load(json_file)

        self.assertEqual(handler.to_json(), json.dumps(expected_json))
