import base64
import json
import os
from unittest import TestCase

from eventhandlers.alerts import AlertEventHandler

script_dir = os.path.dirname(os.path.abspath(__file__))


class TestAlertEventHandler(TestCase):
    def setUp(self):
        with open(os.path.join(script_dir, 'alert_notification.json'), 'r') as json_file:
            self.alert_message = json.load(json_file)

        self.test_event = {
            "data": base64.b64encode(json.dumps(self.alert_message).encode("utf-8"))
        }

    def test_alert_event(self):
        handler = AlertEventHandler(self.test_event)

        self.assertTrue(handler.can_handle_event())

    def test_non_alert_event(self):
        event = {
            "data": base64.b64encode("string message".encode("utf-8"))
        }

        handler = AlertEventHandler(event)

        self.assertFalse(handler.can_handle_event())

    def test_to_json(self):
        handler = AlertEventHandler(self.test_event)

        with open(os.path.join(script_dir, 'alert_notification_adaptive_card.json'), 'r') as json_file:
            expected_json = json.load(json_file)
        self.assertEqual(handler.to_json(), json.dumps(expected_json))
