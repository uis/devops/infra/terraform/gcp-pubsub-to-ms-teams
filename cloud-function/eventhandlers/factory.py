import base64
import binascii
import json
import logging
from .alerts import AlertEventHandler
from .gke import (
    SecurityBulletinEventHandler,
    UpgradeAvailableEventHandler,
    UpgradeEventHandler
)
from .handler import EventHandler

LOG = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class EventHandlerFactory:
    def __init__(self):
        self.event_handler_classes = [
            AlertEventHandler,
            SecurityBulletinEventHandler,
            UpgradeAvailableEventHandler,
            UpgradeEventHandler
        ]

    def log_unknown_event(self, event):
        """Dump out attributes and data for unknown events."""

        attributes = None
        if "attributes" in event:
            attributes = event["attributes"]
        data = event["data"]
        decoded_data = None
        message = None

        try:
            decoded_data = base64.b64decode(event["data"]).decode("utf-8")
            try:
                message = json.loads(decoded_data)
            except json.JSONDecodeError:
                message = "\"decoded_data\" is not valid JSON."
            except Exception as err:
                message = f"Unexpected {err=}, {type(err)=}"
        except binascii.Error:
            decoded_data = "Could not decode \"data\""
        except Exception as err:
            decoded_data = f"Unexpected {err=}, {type(err)=}"

        log_message = (
            "No event handler found for event, "
            f"attributes: {attributes}, "
            f"data: {data}, "
            f"decoded_data: {decoded_data}, "
            f"message: {message}"
        )
        LOG.error(log_message)

    def get_event_handler(self, event) -> EventHandler:
        """
        Get an appropriate EventHandler instance for the given event.

        Args:
            event (dict): The event data.

        Returns:
            EventHandler: An instance of the appropriate EventHandler class if found.

        Raises:
            Exception: If the 'data' attribute is not found in the event payload.
            Exception: If no event handler is found for the event.
        """

        if "data" not in event:
            raise Exception("'data' attribute not found in event payload.")

        for event_handler_class in self.event_handler_classes:
            event_handler = event_handler_class(event)
            if event_handler.can_handle_event():
                return event_handler

        # We don't know how to handle this event.
        self.log_unknown_event(event)
        raise Exception("No event handler found for event.")
