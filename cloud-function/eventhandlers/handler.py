import base64


class EventHandler:
    """
    An abstract class for handling any type of notification event and generating
    MS Teams message card json from that data.
    """

    def __init__(self, event):
        if "attributes" in event:
            self.attributes = event["attributes"]
        else:
            self.attributes = {}
        try:
            if "data" in event:
                # data should be base64 encoded utf-8 string
                self.decoded_data = base64.b64decode(event["data"]).decode("utf-8")
        except Exception:
            # Ignore any errors here - handling is performed by factory class
            self.decoded_data = {}

    def can_handle_event(self) -> bool:
        """
        Placeholder method to determine if the event can be handled.

        This method should be implemented in subclasses to define the conditions under
        which the event can be handled.

        Returns:
            bool: True if the event can be handled, False otherwise.
        """

        pass

    def to_json(self) -> str:
        """
        Placeholder method to return MS Teams adaptive card json.

        This method should be implemented in subclasses to create the appropriate message card
        json string for the notifiction as each notification will contains different data.


        This is a new and preferred schema used by MS Teams. See
        https://learn.microsoft.com/en-us/connectors/teams/?tabs=text1#adaptivecarditemschema for the full reference.

        Returns:
            str: A JSON string representing the MS Teams message card.
        """

        pass
