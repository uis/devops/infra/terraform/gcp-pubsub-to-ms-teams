import logging
import google.auth
import json
from typing import List
from .cards import AdaptiveCard
from .handler import EventHandler
from googleapiclient import discovery

LOG = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class GKEMessageCard(AdaptiveCard):
    def __init__(self, message: str, attributes: dict):
        super().__init__()

        self.message = message
        self.attributes = attributes
        self.payload = json.loads(self.attributes["payload"])
        self._set_common_facts()

    def _set_common_facts(self):
        cluster_location = self.attributes["cluster_location"]
        cluster_name = self.attributes["cluster_name"]
        project_number = self.attributes["project_id"]  # confusingly, this is the project number, not the project id
        cluster_details_base_url = "https://console.cloud.google.com/kubernetes/clusters/details"

        project_info = self._get_project_info(project_number)
        project_name = project_info["display_name"]
        project_id = project_info["project_id"]
        print(project_name)
        print(project_id)

        self.add_fact("Project Name",
                       f"[{project_name}](https://console.cloud.google.com/?project={project_number})")
        self.add_fact("Project Number",
                       f"[{project_number}](https://console.cloud.google.com/?project={project_number})")
        self.add_fact("Project ID",
                       f"[{project_id}](https://console.cloud.google.com/?project={project_number})")
        self.add_fact("Cluster location", self.attributes["cluster_location"])
        self.add_fact("Cluster name", f"[{cluster_name}]({cluster_details_base_url}/{cluster_location}/{cluster_name}/details?project={project_number})")

    def _get_project_info(self, project_number: str) -> dict:
        display_name = ""
        project_id = ""

        try:
            credentials, _ = google.auth.default()
            crm = discovery.build('cloudresourcemanager', 'v3', credentials=credentials)

            project = crm.projects().get(name="projects/" + project_number).execute()
            display_name = project['displayName']
            project_id = project['projectId']
        except Exception as err:
            # Ignore any errors here, we will still output the notification, just missing these details
            err_msg = "Exception occurred while retrieving project details"
            display_name = err_msg
            LOG.error(f"{err_msg}: {err=}")

        return {
            "display_name": display_name,
            "project_id": project_id
        }



class SecurityBulletinMessageCard(GKEMessageCard):
    def __init__(self, message: str, attributes: dict):
        super().__init__(message, attributes)

        self.title = "[Cluster Notification] SecurityBulletinEvent"
        self.summary = self.message
        self.text = self.summary

        cve_ids = ', '.join(self.payload["cveIds"])
        affected_supported_minors = ', '.join(self.payload["affectedSupportedMinors"])
        patched_versions = ', '.join(self.payload["patchedVersions"])

        self.add_fact("Resource Type Affected", self.payload["resourceTypeAffected"])
        self.add_fact("Bulletin Id", self.payload["bulletinId"])
        self.add_fact("CVE Ids", cve_ids)
        self.add_fact("Severity", self.payload["severity"])
        self.add_fact("Description", self.payload["briefDescription"])
        self.add_fact("Affected Supported Minors", affected_supported_minors)
        self.add_fact("Patched Versions", patched_versions)
        self.add_fact("Suggested Upgrade Target", self.payload["suggestedUpgradeTarget"])

        self.action_title = "View Bulletin"
        self.action_url = self.payload["bulletinUri"]


class UpgradeAvailableMessageCard(GKEMessageCard):
    def __init__(self, message: str, attributes: dict):
        super().__init__(message, attributes)

        self.title = "[Cluster Notification] UpgradeAvailableEvent"
        self.summary = self.message
        self.text = self.summary

        self.add_fact("Resource Type", self.payload["resourceType"])
        self.add_fact("Available Version", self.payload["version"])
        self.add_fact("Release Channel", self.payload["releaseChannel"]["channel"])


class UpgradeMessageCard(GKEMessageCard):
    def __init__(self, message: str, attributes: dict):
        super().__init__(message, attributes)

        self.title = "[Cluster Notification] UpgradeEvent"
        self.summary = self.message
        self.text = self.summary

        self.add_fact("Resource Type", self.payload["resourceType"])
        self.add_fact("Operations", self.payload["operation"])
        self.add_fact("Operation Start Time", self.payload["operationStartTime"])
        self.add_fact("Current Version", self.payload["currentVersion"])
        self.add_fact("Target Version", self.payload["targetVersion"])



class GKEEventHandler(EventHandler):
    """
    Base class to handle GKE pubsub notification events.

    See:
    - Go to https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications
    """

    def __init__(self, event):
        super().__init__(event)

        # Data contains human-readable information.
        self.message = self.decoded_data

    def can_handle_event(self) -> bool:
        if "cluster_name" in self.attributes and "type_url" in self.attributes:
            return True
        return False


class SecurityBulletinEventHandler(GKEEventHandler):
    """
    A class to handle GKE pubsub notification SecurityBulletinEvent events.
    See:
    - https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications#securitybulletinevent_2
    """

    def can_handle_event(self) -> bool:
        if super().can_handle_event():
            if self.attributes["type_url"] == "type.googleapis.com/google.container.v1beta1.SecurityBulletinEvent":
                return True
        return False

    def to_json(self) -> str:
        card = SecurityBulletinMessageCard(self.message, self.attributes)

        return card.to_json()


class UpgradeAvailableEventHandler(GKEEventHandler):
    """
    A class to handle GKE pubsub notification UpgradeAvailableEvent events.
    See:
     - https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications#upgradeavailableevent_2
    """

    def can_handle_event(self) -> bool:
        if super().can_handle_event():
            if self.attributes["type_url"] == "type.googleapis.com/google.container.v1beta1.UpgradeAvailableEvent":
                return True
        return False

    def to_json(self) -> str:
        card = UpgradeAvailableMessageCard(self.message, self.attributes)

        return card.to_json()


class UpgradeEventHandler(GKEEventHandler):
    """
    A class to handle GKE pubsub notification UpgradeEvent events.
    See:
     - https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications#upgradeevent_2
    """

    def can_handle_event(self) -> bool:
        if super().can_handle_event():
            if self.attributes["type_url"] == "type.googleapis.com/google.container.v1beta1.UpgradeEvent":
                return True
        return False

    def to_json(self) -> str:
        card = UpgradeMessageCard(self.message, self.attributes)

        return card.to_json()
