import json
from datetime import datetime, timezone
from .cards import AdaptiveCard
from .handler import EventHandler


class Incident:
    """A class representation of the incident object of the pubsub notification schema.

    To see the full schema:

    - Go to https://cloud.google.com/monitoring/support/notification-options#creating_channels
    - Click "Pub/Sub" in the tabbed menu.
    - Click the link in "To understand the data schema, see Schema example".
    """

    def __init__(self, incident):
        self.incident_id = incident["incident_id"]
        self.scoping_project_id = incident["scoping_project_id"]
        self.resource_id = incident["resource_id"]
        self.resource_name = incident["resource_name"]
        self.metric_name = incident["metric"]["displayName"]
        self.state = incident["state"]
        self.started_at = incident["started_at"]
        self.ended_at = incident["ended_at"]
        self.policy_name = incident["policy_name"]
        self.condition_name = incident["condition_name"]
        self.url = incident["url"]
        self.summary = incident["summary"]


class AlertAdaptiveCard(AdaptiveCard):
    def __init__(self, incident):
        super().__init__()

        policy_name = incident.policy_name if incident.policy_name else "UNKNOWN POLICY"
        condition_name = (
            incident.condition_name if incident.condition_name else "UNKNOWN CONDITION"
        )

        if incident.state.lower() == "open":
            self.title = f"[ALERT] {condition_name}"
        else:
            self.title = f"[RESOLVED] {condition_name}"

        self.summary = incident.summary if incident.summary else "No summary available."
        self.text = self.summary

        if incident.started_at is not None and incident.started_at > 0:
            start = datetime.fromtimestamp(incident.started_at, timezone.utc).strftime(
                "%d %b %Y at %I:%M%p %Z"
            )
            self.add_fact("Start time", start)

        if incident.ended_at is not None and incident.ended_at > 0:
            end = datetime.fromtimestamp(incident.ended_at, timezone.utc).strftime(
                "%d %b %Y at %I:%M%p %Z"
            )
            self.add_fact("End time", end)

        self.add_fact("Project", (
                    f"[{incident.scoping_project_id}]"
                    f"(https://console.cloud.google.com/?project={incident.scoping_project_id})"
                ))
        self.add_fact("Policy", policy_name)
        self.add_fact("Condition", condition_name)
        self.add_fact("Metric", incident.metric_name)

        self.action_title = "View Incident"
        self.action_url = incident.url



class AlertEventHandler(EventHandler):
    def __init__(self, event):
        super().__init__(event)
        try:
            self.data = json.loads(self.decoded_data)
        except Exception:
            # data is not a json object
            self.data = {}

    def can_handle_event(self) -> bool:
        if "incident" in self.data:
            return True
        return False

    def to_json(self) -> str:
        incident = Incident(self.data["incident"])
        card = AlertAdaptiveCard(incident)

        return card.to_json()
