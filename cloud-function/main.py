"""Cloud Functions (1st gen) function code to proxy pubsub messages to an MS Teams channel.

This module is intended to be deployed as a Cloud Functions (1st gen) event-driven background function. The Cloud
Function event trigger should be a pubsub topic. The url of an Incoming Webhook connector for the target MS Teams
channel is expected to be available in the MSTEAMS_WEBHOOK_URL environment variable.
"""

import logging
import os
import requests

from eventhandlers.factory import EventHandlerFactory

LOG = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def main(event, context):
    """Cloud Functions handler function."""

    url = os.environ["MSTEAMS_WEBHOOK_URL"]

    factory = EventHandlerFactory()
    event_handler = factory.get_event_handler(event)

    json_str = event_handler.to_json()
    LOG.info(f"Sending message to MS Teams: {json_str}")
    res = requests.post(url, data=json_str, headers={"Content-Type":"application/json"})
    res.raise_for_status()
