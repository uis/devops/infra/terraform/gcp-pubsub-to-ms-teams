# Changelog

## [1.3.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/compare/1.3.1...1.3.2) (2025-01-17)

## [1.3.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/compare/1.3.0...1.3.1) (2025-01-17)

### Bug Fixes

* use dict directly rather horrible string template, as this caused encoding issues ([e9911de](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/e9911decab99a63ac9220a7248bf8d91527a3177))

## [1.3.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/compare/1.2.1...1.3.0) (2024-11-20)

### Features

* add create_default_topic variable to prevent 'Invalid count argument' errors ([abd78e4](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/abd78e49788a8c738888e1fe7321da701726b343))

## [1.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/compare/1.2.0...1.2.1) (2024-10-24)

### Bug Fixes

* don't create resource random_id.topic_name when a topic_id is provided ([beb7ea8](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/beb7ea8db298f236bfdb1893218d812f79fd5fc6))
* pin version of node used for running markdownlint ([47dedb7](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/47dedb7c0a323eb370e425ec4bc6ff3e54fe25e3))

## [1.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/compare/1.1.0...1.2.0) (2024-09-12)


### Features

* Add alert for Cloud Function ([f94590a](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/f94590ab91d3e3f2816b36cc2a14c4b13952acc1))
* added pre-commit and release-it functionality + remove unused class MessageCard ([994efbf](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/994efbf1621fe1528f826edd7daa023900db93b6))
* switch from legacy message cards to adaptive cards ([41a2d71](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/41a2d71877e6bb4d25a159a0d7fa446bba41d34f))


### Bug Fixes

* add missing project on pubsub resources ([29005cc](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-pubsub-to-ms-teams/commit/29005cc06103406da38ddaf38dda39c23c0fa2f6))

## [1.2.0] - 2024-07-17

### Added

- Added alerting policy for pubsub2teams Cloud Function.

### Fixed

- google_pubsub_topic and google_pubsub_topic_iam_member were missing project parameter.

## [1.1.0] - 2024-05-16

### Changed

- Update google provider version constraint to `>= 4.0` to provide compatibility with newer provider versions.

## [1.0.1] - 2023-09-15

### Added

- For GKE notifications, include project display name and project ID in Teams message.

## [1.0.0] - 2023-09-15

### Added

- Initial version
